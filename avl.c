#include "avl.h"

struct avl_node {
	struct avl_node *left;
	struct avl_node *right;
	unsigned int height;
	void *key;
	void *data;
};

struct avl_tree {
	struct avl_node *root;
	size_t keySize;
	size_t dataSize;
	size_t size;
	bool keyFree;
	bool dataFree;
	bool (*compPred)(void *, void *, size_t);
	/* compPred(x, y) === x < y */
	bool (*eqPred)(void *, void *, size_t);
};

struct avl_tree
*avl_tree_init(kSize, dSize, kFree, dFree, cPred, ePred)
size_t kSize;
size_t dSize;
bool kFree;
bool dFree;
bool (*cPred)(void *, void *, size_t);
bool (*ePred)(void *, void *, size_t);
{
	struct avl_tree *temp = malloc(sizeof(avl_tree));
	if (temp == NULL) {
		return NULL;
	}
	temp->root = NULL;
	temp->keySize = kSize;
	temp->dataSize = dSize;
	temp->keyFree = kFree;
	temp->dataFree = dFree;
	temp->compPred = cPred;
	temp->eqPred = ePred;
	temp->size = 0;
	return temp;
}

static void avl_node_free(node, kFree, dFree)
struct avl_node *node;
bool kFree;
bool dFree;
{
	if (node != NULL) {
		avl_node_free(node->left, kFree, dFree);
		avl_node_free(node->right, kFree, dFree);
		if (kFree) {
			free(node->key);
		}
		if (dFree) {
			free(node->data);
		}
		free(node);
	}
}

void
avl_tree_free(tree)
struct avl_tree *tree;
{
	avl_node_free(tree->root, tree->keyFree, tree->dataFree);
	free(tree);
}

static size_t
avl_node_getsize(node)
struct avl_node *node;
{
	if (node == NULL) {
		return 0;
	}
	else {
		return avl_node_getsize(node->left)
		     + avl_node_getsize(node->right);
	}
}

void
avl_tree_updatesize(tree)
struct avl_tree *tree;
{
	if (tree != NULL) {
		tree->size = avl_node_getsize(tree->root);
	}
}

size_t
avl_tree_getsize(tree)
struct avl_tree *tree;
{
	if (tree == NULL) {
		return 0;
	}
	else {
		return tree->size;
	}
}

static unsigned int
avl_node_getheight(node)
struct avl_node *node;
{
	if (node == NULL) {
		return 0;
	}
	return node->height;
}

static void
avl_node_updateheight(node)
struct avl_node *node;
{
	 if (node != NULL) {
	 	node->height = max(avl_node_getheight(node->left),
				   avl_node_getheight(node->right))
			       + 1;
	 }
}

static void
swapptrs(x, y)
void **x;
void **y;
{
	void *temp = *x;
	*x = *y;
	*y = temp;
}

static void
avl_node_leftrotate(node)
struct avl_node *node;
{
	if (node != NULL && node->right != NULL) {
		struct avl_node *temp = node->right;
		swapptrs(&(node->left), &(node->right));
		swapptrs(&(node->right), &(temp->left));
		swapptrs(&(temp->right), &(node->right));
		swapptrs(&(node->data), &(temp->data));
		swapptrs(&(node->key), &(temp->key));
		avl_node_updateheight(node);
		avl_node_updateheight(temp);
	}
}

static void
avl_node_rightrotate(node)
struct avl_node *node;
{
	if (node != NULL && node->left != NULL) {
		struct avl_node *temp = node->left;
		swapptrs(&(node->left), &(node->right));
		swapptrs(&(node->left), &(temp->right));
		swapptrs(&(temp->left), &(node->left));
		swapptrs(&(node->data), &(temp->data));
		swapptrs(&(node->key), &(temp->key));
		avl_node_updateheight(node);
		avl_node_updateheight(temp);
	}
}

static void
avl_node_balance(node)
struct avl_node *node;
{
	if (node != NULL) {
		avl_node_updateheight(node);
		if (avl_node_getheight(node->left) > 1 &&
		    avl_node_getheight(node->left)
		  - avl_node_getheight(node->right) >= 2) {
			struct avl_node *lnode = node->left;
			if (avl_node_getheight(lnode->left)
			  > avl_node_getheight(lnode->right)) {
				avl_node_rightrotate(node);
			}
			else if (avl_node_getheight(lnode->left)
			       < avl_node_getheight(lnode->right)) {
				avl_node_leftrotate(lnode);
				avl_node_rightrotate(node);
			}
		}
		else if (avl_node_getheight(node->right) > 1 &&
			 avl_node_getheight(node->right) 
		       - avl_node_getheight(node->left) >= 2) {
			struct avl_node *rnode = node->right;
			if (avl_node_getheight(rnode->left)
			  > avl_node_getheight(rnode->right)) {
				avl_node_rightrotate(rnode);
				avl_node_leftrotate(node);
			}
			else if (avl_node_getheight(rnode->left)
			       < avl_node_getheight(rnode->right)) {
				avl_node_leftrotate(node);
			}
		}
	}
}

static int
avl_node_insert(node, key, data, compPred, keySize)
struct avl_node *node;
void *key;
void *data;
bool (*compPred)(void *, void *, size_t);
size_t keySize;
{
	int status;
	if (node == NULL) {
		return AVL_FAILURE;
	}
	else if ((*compPred)(key, node->key, keySize)) {
		if (node->left != NULL) {
			status = avl_node_insert(node->left,
						 key,
						 data,
						 compPred,
						 keySize);
		}
		else {
			node->left = malloc(sizeof(struct avl_node));
			if (node->left == NULL) {
				return AVL_FAILURE;
			}
			(node->left)->left = NULL;
			(node->left)->right = NULL;
			(node->left)->key = key;
			(node->left)->data = data;
			(node->left)->height = 1;
			status = AVL_SUCCESS;
		}
	}
	else {
		if (node->right != NULL) {
			status = avl_node_insert(node->right,
						 key,
						 data,
						 compPred,
						 keySize);
		}
		else {
			node->right = malloc(sizeof(struct avl_node));
			if (node->right == NULL) {
				return AVL_FAILURE;
			}
			(node->right)->left = NULL;
			(node->right)->right = NULL;
			(node->right)->key = key;
			(node->right)->data = data;
			(node->right)->height = 1;
			status = AVL_SUCCESS;
		}
	}
	if (status == AVL_FAILURE) {
		return AVL_FAILURE;
	}
	avl_node_balance(node);
	return AVL_SUCCESS;
}

static int
avl_node_insertonce(node, key, data, compPred, eqPred, keySize)
struct avl_node *node;
void *key;
void *data;
bool (*compPred)(void *, void *, size_t);
bool (*eqPred)(void *, void *, size_t);
size_t keySize;
{
	int status;
	if (node == NULL) {
		return AVL_FAILURE;
	}
	else if ((*compPred)(key, node->key, keySize)) {
		if (node->left != NULL) {
			status = avl_node_insertonce(node->left,
						     key,
						     data,
						     compPred,
						     eqPred,
						     keySize);
		}
		else {
			node->left = malloc(sizeof(struct avl_node));
			if (node->left == NULL) {
				return AVL_FAILURE;
			}
			(node->left)->left = NULL;
			(node->left)->right = NULL;
			(node->left)->key = key;
			(node->left)->data = data;
			(node->left)->height = 1;
			status = AVL_ABSENT;
		}
	}
	else if ((*eqPred)(key, node->key, keySize)) {
		status = AVL_PRESENT;
	}
	else {
		if (node->right != NULL) {
			status = avl_node_insertonce(node->right,
						     key,
						     data,
						     compPred,
						     eqPred,
						     keySize);
		}
		else {
			node->right = malloc(sizeof(struct avl_node));
			if (node->right == NULL) {
				return AVL_FAILURE;
			}
			(node->right)->left = NULL;
			(node->right)->right = NULL;
			(node->right)->key = key;
			(node->right)->data = data;
			(node->right)->height = 1;
			status = AVL_ABSENT;
		}
	}

	if (status != AVL_FAILURE) {
		avl_node_balance(node);
	}
	return status;
}

int
avl_tree_insert(tree, key, data)
struct avl_tree *tree;
void *key;
void *data;
{
	if (tree == NULL) {
		return AVL_FAILURE;
	}
	else if (tree->root == NULL) {
		tree->root = malloc(sizeof(struct avl_node));
		if (tree->root == NULL) {
			return AVL_FAILURE;
		}
		(tree->root)->left = NULL;
		(tree->root)->right = NULL;
		(tree->root)->key = key;
		(tree->root)->data = data;
		(tree->root)->height = 1;
		tree->size++;
		return AVL_SUCCESS;
	}
	else {
		int status = avl_node_insert(tree->root,
					     key,
					     data,
					     tree->compPred,
					     tree->keySize);
		if (status != AVL_FAILURE) {
			tree->size++;
		}
		return status;
	}
}

int
avl_tree_insertonce(tree, key, data)
struct avl_tree *tree;
void *key;
void *data;
{
	if (tree == NULL) {
		return AVL_FAILURE;
	}
	else if (tree->root == NULL) {
		tree->root = malloc(sizeof(struct avl_node));
		if (tree->root == NULL) {
			return AVL_FAILURE;
		}
		(tree->root)->left = NULL;
		(tree->root)->right = NULL;
		(tree->root)->key = key;
		(tree->root)->data = data;
		(tree->root)->height = 1;
		tree->size++;
		return AVL_ABSENT;
	}
	else {
		int status = avl_node_insertonce(tree->root,
						 key,
						 data,
						 tree->compPred,
						 tree->eqPred,
						 tree->keySize);
		if (status == AVL_ABSENT) {
			tree->size++;
		}
		return status;
	}
}

int
avl_tree_search(tree, key, dataPtr)
struct avl_tree *tree;
void *key;
void **dataPtr;
{
	struct avl_node *temp;
	if (tree == NULL) {
		return AVL_FAILURE;
	}
	temp = tree->root;
	while (temp != NULL) {
		if ((*tree->eqPred)(key, temp->key, tree->keySize)) {
			if (dataPtr != NULL) {
				*dataPtr = temp->data;
			}
			return AVL_PRESENT;
		}
		else if ((*tree->compPred)(key,
					   temp->key,
					   tree->keySize)) {
			temp = temp->left;
		}
		else {
			temp = temp->right;
		}
	}
	return AVL_ABSENT;
}

static int
avl_node_insertnodeleft(parent, subtree)
struct avl_node *parent;
struct avl_node *subtree;
{
	if (parent == NULL) {
		return AVL_FAILURE;
	}
	else if (parent->left == NULL) {
		parent->left = subtree;
	}
	else {
		int status = avl_node_insertnodeleft(parent->left,
						     subtree);
		if (status == AVL_FAILURE) {
			return AVL_FAILURE;
		}
	}	
	avl_node_balance(parent);
	return AVL_SUCCESS;
}

static int
avl_node_delete(node,
		dir,
		kFree,
		dFree,
		key,
		compPred, 
		eqPred,
		keySize)
struct avl_node *node;
signed char dir;
bool kFree;
bool dFree;
void *key;
bool (*compPred)(void *, void *, size_t);
bool (*eqPred)(void *, void *, size_t);
size_t keySize;
{
	struct avl_node *temp;
	int status;
	if (node == NULL || (dir != -1 && dir != 1)) {
		return AVL_FAILURE;
	}
	temp = dir == -1 ? node->left : node->right;
	if (temp == NULL) {
		return AVL_ABSENT;
	}
	else if ((*eqPred)(key, temp->key, keySize)) {
		if (dir == -1) {
			node->left = temp->right;
			status = avl_node_insertnodeleft(node->left,
						     temp->left);
		}
		else {
			node->right = temp->right;
			status = avl_node_insertnodeleft(node->right,
						     temp->left);
		}

		if (status == AVL_FAILURE) {
			return AVL_FAILURE;
		}
		status = AVL_PRESENT;
		if (kFree) {
			free(temp->key);
		}
		if (dFree) {
			free(temp->data);
		}
		free(temp);
	}
	else if ((*compPred)(key, temp->key, keySize)) {
		status = avl_node_delete(temp,
					 -1,
					 kFree,
					 dFree,
					 key,
					 compPred,
					 eqPred,
					 keySize);
	}
	else {
		status = avl_node_delete(temp,
					 1,
					 kFree,
					 dFree,
					 key,
					 compPred,
					 eqPred,
					 keySize);
	}
	if (status == AVL_FAILURE) {
		return AVL_FAILURE;
	}
	avl_node_balance(node);
	return status;
}

int avl_tree_delete(tree, key)
struct avl_tree *tree;
void *key;
{
	struct avl_node *temp;
	int status;
	if (tree == NULL) {
		return AVL_FAILURE;
	}
	temp = tree->root;
	if (temp == NULL) {
		return AVL_ABSENT;
	}
	else if ((*tree->eqPred)(temp->key, key, tree->keySize)) {
		int status;
		tree->root = temp->right;
		status = avl_node_insertnodeleft(tree->root,
						 temp->left);
		if (status == AVL_FAILURE) {
			return AVL_FAILURE;
		}
		if (tree->keyFree) {
			free(temp->key);
		}
		if (tree->dataFree) {
			free(temp->data);
		}
		free(temp);
		tree->size--;
		return AVL_PRESENT;
	}
	else if ((*tree->compPred)(key, temp->key, tree->keySize)) {
		status = avl_node_delete(temp,
					     -1,
					     tree->keyFree,
					     tree->dataFree,
					     key,
					     tree->compPred,
					     tree->eqPred,
					     tree->keySize);
	}
	else {
		status = avl_node_delete(temp,
					     1,
					     tree->keyFree,
					     tree->dataFree,
					     key,
					     tree->compPred,
					     tree->eqPred,
					     tree->keySize);
	
	}
	if (status == AVL_PRESENT) {
		tree->size--;
	}
	return status;
}

static void
avl_node_inorder(node, outFunc, outPtr, keySize, dataSize)
struct avl_node *node;
void (*outFunc)(void *, void *, size_t, size_t, void *);
void *outPtr;
size_t keySize;
size_t dataSize;
{
	if (node != NULL) {
		avl_node_inorder(node->left,
				 outFunc,
				 outPtr,
				 keySize,
				 dataSize);
		(*outFunc)(node->key,
			   node->data,
			   keySize,
			   dataSize,
			   outPtr);
		avl_node_inorder(node->right,
				 outFunc,
				 outPtr,
				 keySize,
				 dataSize);
	}
}

int
avl_tree_inorder(tree, outFunc, outPtr)
struct avl_tree *tree;
void (*outFunc)(void *, void *, size_t, size_t, void *);
void *outPtr;
{
	if (tree == NULL) {
		return AVL_FAILURE;
	}
	avl_node_inorder(tree->root,
			 outFunc,
			 outPtr,
			 tree->keySize,
			 tree->dataSize);
	return AVL_SUCCESS;
}

static void
avl_node_preorder(node, outFunc, outPtr, keySize, dataSize)
struct avl_node *node;
void (*outFunc)(void *, void *, size_t, size_t, void *);
void *outPtr;
size_t keySize;
size_t dataSize;
{
	if (node != NULL) {
		(*outFunc)(node->key,
			   node->data,
			   keySize,
			   dataSize,
			   outPtr);
		avl_node_preorder(node->left,
				  outFunc,
				  outPtr,
				  keySize,
				  dataSize);
		avl_node_preorder(node->right,
				  outFunc,
				  outPtr,
				  keySize,
				  dataSize);
	}
}

int
avl_tree_preorder(tree, outFunc, outPtr)
struct avl_tree *tree;
void (*outFunc)(void *, void *, size_t, size_t, void *);
void *outPtr;
{
	if (tree == NULL) {
		return AVL_FAILURE;
	}
	avl_node_preorder(tree->root,
			  outFunc,
			  outPtr,
			  tree->keySize,
			  tree->dataSize);
	return AVL_SUCCESS;
}

static void
avl_node_postorder(node, outFunc, outPtr, keySize, dataSize)
struct avl_node *node;
void (*outFunc)(void *, void *, size_t, size_t, void *);
void *outPtr;
size_t keySize;
size_t dataSize;
{
	if (node != NULL) {	
		avl_node_postorder(node->left,
				   outFunc,
				   outPtr,
				   keySize,
				   dataSize);
		avl_node_postorder(node->right,
				   outFunc,
				   outPtr,
				   keySize,
				   dataSize);
		(*outFunc)(node->key,
			   node->data,
			   keySize,
			   dataSize,
			   outPtr);
	}
}

int
avl_tree_postorder(tree, outFunc, outPtr)
struct avl_tree *tree;
void (*outFunc)(void *, void *, size_t, size_t, void *);
void *outPtr;
{
	if (tree == NULL) {
		return AVL_FAILURE;
	}
	avl_node_postorder(tree->root,
			 outFunc,
			 outPtr,
			 tree->keySize,
			 tree->dataSize);
	return AVL_SUCCESS;
}
