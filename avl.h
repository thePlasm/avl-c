#ifndef __AVL_H
#define __AVL_H

#include <stdlib.h>
#include <stdbool.h>

#define AVL_FAILURE 0
#define AVL_SUCCESS 1
#define AVL_PRESENT 2
#define AVL_ABSENT 3

#define max(X, Y) ((X) < (Y) ? (Y) : (X))

typedef struct avl_tree avl_tree;
typedef struct avl_node avl_node;

struct avl_tree
*avl_tree_init(size_t kSize,
	       size_t dSize,
	       bool kFree,
	       bool dFree,
	       bool (*cPred)(void *, void *, size_t),
	       bool (*ePred)(void *, void *, size_t));

void
avl_tree_free(struct avl_tree *tree);

size_t
avl_tree_getsize(struct avl_tree *tree);

void
avl_tree_updatesize(struct avl_tree *tree);

int
avl_tree_insert(struct avl_tree *tree,
		void *key,
		void *data);

int
avl_tree_insertonce(struct avl_tree *tree,
		    void *key,
		    void *data);

int
avl_tree_search(struct avl_tree *tree,
		void *key,
		void **dataPtr);

int
avl_tree_delete(struct avl_tree *tree,
		void *key);

int
avl_tree_inorder(struct avl_tree *tree,
		 void (*outFunc)(void *,
		 		 void *,
				 size_t,
				 size_t,
				 void *),
		 void *outPtr);

int
avl_tree_preorder(struct avl_tree *tree,
		  void (*outFunc)(void *,
		 		  void *,
				  size_t,
				  size_t,
				  void *),
		  void *outPtr);

int
avl_tree_postorder(struct avl_tree *tree,
		   void (*outFunc)(void *,
		 		   void *,
				   size_t,
				   size_t,
				   void *),
		   void *outPtr);

#endif
