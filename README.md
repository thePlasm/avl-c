# avl-c
## description
An implementation of an AVL tree in C. An AVL tree is a type of rebalancing binary tree that ensures that the worst-case time complexity for accesses, deletions and insertions is O(log n).
## motivation
I wanted to be able to use more efficient data structures in my projects and I also wanted to be able to apply my study into self-balancing trees.
